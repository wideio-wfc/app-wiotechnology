# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import os
import datetime
import zipfile
import settings
import json
import bson
import uuid
import grp
import datetime
import subprocess
import time
import shutil
import socket
from django.core.urlresolvers import reverse
#
from django.contrib.auth.models import AbstractUser  # , User
import wioframework.fields as models
from wioframework.amodels import *
from wioframework import decorators as dec
from django.http import HttpResponseRedirect

MODELS = []


@wideio_commentable()
@wideio_tagged()
@wideio_owned()
@wideio_publishable()
@wideiomodel
class IPAsset(models.Model):
    name = models.CharField(max_length=255, blank=False, db_index=True)
    ip_type = models.CharField(max_length=255, blank=False, choices=(
        ("Patent", "Patent"), ("Copyright", "Copyright"), ("Trademark", "Trademark"),
    )
                               )
    image = models.ForeignKey('references.Image')
    registration_data = models.DateTimeField()
    renewal_date = models.DateTimeField()
    private = models.OneToOneField('technology.IPAssetPrivateDetails')

    def get_all_references(self, st):
        return [self.private, self.image]

    # managed by STAFF

    class WIDEIO_Meta:
        NO_DRAFT = True
        permissions = dec.all_permissions_from_request_rwa(
            lambda r: True,
            lambda r: r.user.is_staff,
            lambda r: r.user.is_staff)


MODELS.append(IPAsset)


@wideio_commentable()
@wideio_tagged()
@wideio_owned()
@wideio_publishable()
@wideiomodel
class IPAssetPrivateDetails(models.Model):
    price = models.FloatField()

    def get_all_references(self, st):
        return []

    class WIDEIO_Meta:
        NO_DRAFT = True
        permissions = dec.all_permissions_from_request_rwa(
            lambda r: True,
            lambda r, o: r.user in o.owner.get_members(),
            lambda r, o: r.user in o.owner.get_members())


MODELS.append(IPAssetPrivateDetails)
